<?php

function conectar() {
    $mysqli = mysqli_connect("localhost", "root", "1234", "prestamos");
    if ($mysqli->connect_errno) {
        echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }

    return $mysqli;
}

function ejecuta($sql) {

    $mysqli = conectar();
    $resultado = mysqli_query($mysqli, $sql);
    $fila = mysqli_fetch_assoc($resultado);
    return $fila;
}

function ejecuta2($sql) {
    $mysqli = conectar();
    mysqli_query($mysqli, $sql);
    //$fila = mysqli_fetch_assoc($resultado);
}

function ejecuta_combo($sql) {
    $mysqli = conectar();
    $resultado = mysqli_query($mysqli, $sql);
    return $resultado;
}

function ejecuta_tabla($sql) {
    $mysqli = conectar();
    $resultado = mysqli_query($mysqli, $sql);
    return $resultado;
}

function llenar_combobox2($sql, $id, $description) {
    $result = ejecuta_combo($sql);
    $i = 0;
    //$combo = "<option value=0>Seleccione...</option>";
    $combo="";
    while ($row = mysqli_fetch_assoc($result)) {
        $combo = $combo . "<option value=" . $row[$id] . ">" . $row[$description] . "</option>\n";
        $i++;
    }
    return $combo;
}

function maximo_tabla($campo, $tabla) {

    $sql = "SELECT IFNULL(MAX(" . $campo . "),0)as max FROM " . $tabla . ";";

    $datos_codigo = ejecuta($sql);
    $codigo = $datos_codigo['max'] + 1;

    return $codigo;
}

function contar_datos($campo, $tabla, $campo_usar, $id) {
    $sql = 'select ifnull(count(' . $campo . '),0) as c from ' . $tabla . ' where ' . $campo_usar . '=' . $id . ';';
    //$sql="select 1 as c";
    $cont = ejecuta_combo($sql);

    return $cont['c'] > 0;
}

function llena_combobox3($sql, $id, $description, $idbuscar) {
    $result = ejecuta_combo($sql);
    $i = 0;
    $combo = "<option value=>Seleccione...</option>";

    while ($row = mysqli_fetch_assoc($result)) {
        if ($row[$id] == $idbuscar)
            $combo = $combo . "<option value=" . $row[$id] . " selected>" . $row[$description] . "</option>\n";
        else
            $combo = $combo . "<option value=" . $row[$id] . ">" . $row[$description] . "</option>\n";
        $i++;
    }

    return $combo;
}

//Funcion de insertar que devuelve objeto con detalles
function insertar_datos($table, $post_data) {

    $cols = array();
    $vals = array();
    $update = array();

    foreach ($post_data as $key => $value) {
        $cols[] = addslashes($key);
        $vals[] = addslashes($value);
        $update[] = addslashes($key) . "='" . addslashes($value) . "'";
    }
    
    //trim last comma
    $columns = implode(",", $cols);
    $values = "'" . implode("','", $vals) . "'";
    $update_string = implode(", ", $update);

    //build SQL, taking into account that if duplicate entry exists we will update instead.
    $sql = "INSERT INTO " . $table . " (" . $columns . ") VALUES (" . $values . ")
			ON DUPLICATE KEY UPDATE " . $update_string;

    $query = ejecuta2($sql);

    return array(
        'columns' => $columns,
        'vals' => $values,
        'sql' => $sql,
        'query' => $query //returns true or the error
    );
}

function consulta_cliente() {
    $sql = "select c.cod_cli,CONCAT(c.nom_cli,' ',c.ape_cli)as nomcli,c.nom_cli,c.ape_cli,c.dir_cli, c.doc_cli, c.tel_cli, c.cel_cli, c.cor_cli,c.est_cli,t.des_tip "
            . "from clientes c inner join tipos t on c.est_cli=t.cod_tip_cod and t.cod_tip=2";

    $datos = ejecuta_tabla($sql);

    $tabla = "";
    if (count($datos) == 0) {
        $tabla ="";
    } else {
        while ($row = mysqli_fetch_array($datos)) {
            $tabla .= '<tr class="'.$row['est_cli'].'">
                <td>' . $row['cod_cli'] . '</td>
                <td>' . $row['nomcli'] . '</td>
                <td>' . $row['tel_cli'] . '</td>
                <td>' . $row['doc_cli'] . '</td>
                <td>' . $row['cor_cli'] . '</td>
                <td style="text-align:center;">' . $row['des_tip'] . '</td>
                 <td style="text-align:center;"><a type="button" class="btn btn-primary btn-sm" Onclick="editar(this);"
                 data-codigo="'.$row['cod_cli'].'" data-nombre="'.$row['nom_cli'].'" data-telefono="'.$row['tel_cli'].'" 
                 data-doc="'.$row['doc_cli'].'" data-correo="'.$row['cor_cli'].'" data-estado="'.$row['est_cli'].'"  
                 data-apellido="'.$row['ape_cli'].'" data-celular="'.$row['cel_cli'].'" data-direccion="'.$row['dir_cli'].'"
                >Editar</a></td>
        </tr>';
        }
    }
    return $tabla;
}

function status($status){
    if($status!=1){
        return "danger";
    }
    else{
        return "success";
    }
}

?>