<?php

session_start();

if(isset($_SESSION['nombre'])==false){
    header("Location:../pages/login.php");
}
else{
    session_destroy();
    header("Location:../pages/login.php");
}

?>
