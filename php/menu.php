<?php
session_start();

if (isset($_SESSION['nombre']) == false) {
    header("Location:..\pages\login.php");
}
?>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>IMendoza Software</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/jquery-ui.css">
        <link rel="stylesheet" href="../css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/ionicons.min.css">
        <link rel="stylesheet" href="../css/AdminLTE.min.css">
        <link rel="stylesheet" href="../css/_all-skins.min.css">
        <link rel="stylesheet" href="../css/dataTables.bootstrap.css">
        <link rel="stylesheet" href="../css/dataTables.responsive.css">
        <link rel="stylesheet" href="../css/ExportarDataTable.css">
		<link rel="stylesheet" href="../css/buttons_export_datatable.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <header class="main-header">
                <a href="#" class="logo">
                    <span class="logo-mini"><b>I</b>S</span>
                    <span class="logo-lg"><b>IMendoza </b>Software</span>
                </a>
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="../img/login2.png" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo $_SESSION['nombre']; ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>

            <aside class="main-sidebar">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="../img/login2.png" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $_SESSION['nombre']; ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="header">MENU DE NAVEGACION</li>
                        <li>
                            <a href="../pages/Inicio.php">
                                <i class="fa fa-asterisk"></i> <span>Inicio</span>
                            </a>
                        </li>
                        <li>
                            <a href="../pages/Clientes.php">
                                <i class="fa fa-address-card-o"></i> <span>Clientes</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-bank"></i>
                                <span>Bancos</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-circle-o"></i> Registrar Bancos</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Cuentas de Bancos</a></li>
                            </ul>
                        </li>
                        <li class="header">PRESTAMOS</li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-archive"></i>
                                <span>Registros</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-circle-o"></i> Registrar T. Prestamos</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Registrar Garantia</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Registrar F. Pagos</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-money"></i> <span>Prestamos</span>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#">
                                <i class="fa fa-registered"></i> <span>Cobros</span>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#">
                                <i class="fa fa-columns"></i> <span>Cotizaciones</span>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#">
                                <i class="fa fa-book"></i> <span>Cuadres de Caja</span>
                            </a>
                        </li>
                        
                        <li class="header">CONFIGURACIONES</li>
                        <li><a href="#"><i class="fa fa-circle-o text-green"></i> <span>Empresa</span></a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Sucursales</span></a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Información</span></a></li>
                        <li><a href="../php/cerrar_sesion.php"><i class="fa fa-circle-o text-red"></i> <span>Cerrar Sesión</span></a></li>
                    </ul>
                </section>
            </aside>

            <div class="content-wrapper">



