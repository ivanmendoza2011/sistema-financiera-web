<?php


/**
 * Plantilla de metodos reutilizables para manejar los script de la base de datos
 *
 * @author Wandy
 */

abstract class absCRUD {
    
    abstract function select();
    abstract function selectById($id);
    abstract function insert($obj);
    abstract function update($obj);
    abstract function delete();    
    abstract function getUltimoIdInsertado();    
    
}
