<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FormaPago
 *
 * @author Wandy
 */

//include_once '../../data/querys/QsFormaPago.php';


class FormaPago 
{

    private $cod_form;
    private $des_form;
    private $cant_dias;
    private $est_form;

    public function __construct($des_form, $cant_dias, $est_form, $cod_form = null)
    {

        $this->cod_form = $cod_form;
        $this->des_form = $des_form;
        $this->cant_dias = $cant_dias;
        $this->est_form = $est_form;
        
    }

    public function getCodForm()
    {
        return $this->cod_form;
    }

    public function getDesForm()
    {
        return $this->des_form;
    }

    public function getCantDias()
    {
        return $this->cant_dias;
    }

    public function getEstForm()
    {
        return $this->est_form;
    }

    public function setDesForm($desForm)
    {
        $this->des_form = $desForm;
    }

    public function setCantDias($cantDias)
    {
        $this->cant_dias = $cantDias;
    }

    public function setEstForm($estForm)
    {
        $this->est_form = $estForm;
    }
    
    public function __toString() {
        
        return 'Codigo: ' . $this->getCodForm() . ' Descripcion: ' . $this->getDesForm() . ' Cantidad de dias: ' . $this->getCantDias() . ' Estado: ' . $this->getEstForm();
    }

} // FIN CLASE

