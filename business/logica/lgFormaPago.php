<?php

include_once '../entidades/FormaPago.php';
include_once '../../data/DAO/FormaPagoDAO.php';
        

try {
    
    switch ($_POST['opc']) {
    
        case 's' :
            selectFormasPago();            
            break;
        
        case 's_id' :
            selectById();
            break;
        
        case 'u' :       
            actualizarFormasPago();            
            break;
        
        case 'i' :
            insertarFormasPagos();            
            break;

        case 'd' :
            $fpDAO->delete();
            break;

        default : 'Error...! '; // Manejo de errores.
    }

} catch (Exception $ex) {
    echo 'Error!!! ' . $ex->getMessage();
}

// Metodos Genericos

function selectFormasPago() {
    
    $fpDAO = new FormaPagoDAO();
    $qry = $fpDAO->select();
    echo convertToJsonData($qry);
    
}

function selectById() {    
    
    $fpDAO = new FormaPagoDAO();
    $fp = new FormaPago($_POST['des_form'], $_POST['cant_dias'], $_POST['est_form'], $_POST['cod_form']);
    $qry = $fpDAO->selectById($fp->getCodForm());
}

function insertarFormasPagos() {
    
    $fpDAO = new FormaPagoDAO();
    $fp = new FormaPago($_POST['des_form'], $_POST['cant_dias'], $_POST['est_form'], $_POST['cod_form']);
    $fpDAO->insert($fp);
    
}

function actualizarFormasPago() {

    $fpDAO = new FormaPagoDAO();
    $fp = new FormaPago($_POST['des_form'], $_POST['cant_dias'], $_POST['est_form'], $_POST['cod_form']);
    $fpDAO->update($fp);
    
}

// Generalizar este metodo.
function convertToJsonData($qry) {
    
    foreach ($qry as $rows){
                $data[] = $rows;
            }
              $response = array(
                "data" => $data
            );
                       
            header('Content-Type: application/json');
            return json_encode($response);
            
}


//echo $fp->__toString();
