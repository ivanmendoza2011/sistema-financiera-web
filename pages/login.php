<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>IMendoza Software</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/AdminLTE.min.css">
        <link rel="stylesheet" href="../css/blue.css">
        <link rel="stylesheet" href="../css/alertify.min.css">  
        <link rel="stylesheet" href="../css/default.min.css">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="#"><b>IMendoza Software</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Iniciar Sesión</p>

                <form method="post" class="iniciar">
                    <div class="form-group has-feedback">
                        <label>Nombre de Usuario</label>
                        <input type="text" class="form-control" placeholder="Usuario" name="txtusu" id="usu" required>
                    </div>
                    <div class="form-group has-feedback">
                        <label>Contraseña</label>
                        <input type="password" class="form-control" placeholder="Contraseña" name="txtcont" id="cont" required>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-primary btn-block btn-flat" id="iniciar">Iniciar Sesión</button>
                        </div>
                    </div>
                </form> 
            </div>
        </div>

        <script src="../js/jquery-3.2.1.js"></script>
        <script src="../js/jquery-ui.min.js"></script>
        <script src="../js/alertify.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#iniciar').click(function (e) {
                    e.preventDefault();
                    var usu = $('#usu').val();
                    var cont = $('#cont').val();
                    $.post("../php/existe_usuarios.php", {'txtusu': usu, 'txtcont': cont}, function (data) {
                        data = JSON.parse(data);
                        if (data.success==true) {
                            window.location.href = 'sel_sucursal.php';
                        }
                        else if(data.success==="new"){
                                window.location.href = 'crear_empresa.php';
                            }
                        else {
                            var alert = alertify.alert("Información", "Error al Iniciar, Por Favor Contacte al Gerente o al Ingeniero").set('label', 'Aceptar');
                                    alert.set({transition: 'zoom'}); //slide, zoom, flipx, flipy, fade, pulse (default)
                            alert.set('modal', false);  //al pulsar fuera del dialog se cierra o no
                        }
                    });
                });
            });
        </script>
    </body>
</html>
