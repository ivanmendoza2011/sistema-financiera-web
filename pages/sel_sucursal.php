<?php

include '../php/utilidades.php';

session_start();

$usu=$_SESSION['codigo'];

$sql="SELECT * FROM `usuarios_vs_sucursal` where cod_usu='$usu'";
$datos= ejecuta($sql);

if(count($datos)>1){
    $_SESSION['sucursal']=$datos['num_suc'];
    header('Location:../pages/Inicio.php');
}

$combo= llenar_combobox2("select * from sucursal where est_suc=1", "num_suc", "nom_suc");

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>IMendoza Software</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/AdminLTE.min.css">
        <link rel="stylesheet" href="../css/blue.css">
        <link rel="stylesheet" href="../css/alertify.min.css">  
        <link rel="stylesheet" href="../css/default.min.css">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="#"><b>Sistema Financiero</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Seleccionar Sucursal</p>

                <form method="post" class="iniciar" action="ir_menu.php">
                    <div class="form-group has-feedback" >
                        <label>Sucursales</label>
                        <select name="cmbsuc" id="cmbsuc">
                            <?php echo $combo; ?>
                        </select>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <input type="submit" class="btn btn-primary btn-block btn-flat" value="Seleccionar">
                        </div>
                    </div>
                </form> 
            </div>
        </div>

        <script src="../js/jquery-3.2.1.js"></script>
        <script src="../js/jquery-ui.min.js"></script>
        <script src="../js/alertify.min.js"></script>
    </body>
</html>