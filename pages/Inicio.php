<?php
include '../php/utilidades.php';
include '../php/menu.php';

$sql = "select count(*)as c from usuarios where est_usu=1;";
$datos_usu = ejecuta($sql);

$sql="select count(*)as c1 from clientes";
$datos_cli= ejecuta($sql);

?>

<section class="content-header">
    <h1>
        Inicio <small>Panel de Control</small>
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?php echo $datos_cli['c1']; ?></h3>
                    <p>Clientes</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="Clientes.php" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>0</h3>

                    <p>Prestado</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?php echo $datos_usu['c']; ?></h3>

                    <p>Usuarios</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="#" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>$0.00</h3>

                    <p>Cobrado en el Mes</p>
                </div>
                <div class="icon">
                    <i class="ion ion-cash"></i>
                </div>
                <a href="#" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Clientes a Cobrar</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table width="100%" id="example1" class="table table-bordered table-striped table-hover table-condensed">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nombre</th>
                        <th>Teléfono</th>
                        <th>Monto a Cobrar</th>
                        <th>Balance</th>
						<th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</section>

<?php
include '../php/script.php';
include '../php/footer.php';
?>

<script>
    $(document).ready(function () {
        $('#example1').DataTable({
            responsive: true,
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_  Clientes",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ninguna Información disponible en estos momentos.",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ Clientes",
                "sInfoEmpty": "Mostrando Clientes del 0 al 0 de un total de 0 Clientes",
                "sInfoFiltered": "(filtrado de un total de _MAX_ Clientes)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
    });
</script>
