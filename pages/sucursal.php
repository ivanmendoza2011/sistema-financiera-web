<?php
session_start();
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>IMendoza Software</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/AdminLTE.min.css">
        <link rel="stylesheet" href="../css/blue.css">
        <link rel="stylesheet" href="../css/alertify.min.css">  
        <link rel="stylesheet" href="../css/default.min.css">
    </head>
    <body class=" login-page">      
        <div class="login-box-msg">`
            <div class="login-logo">
                <a href="#"><b>IMendoza Software Crear Sucursal</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <form method="post" class="iniciar">
                    <div class="form-group has-feedback">
                        <label>Nombre de Empresa</label>
                        <input type="text" class="form-control" placeholder="Nombre" name="txtemp" id="nom" required>
                    </div>

                    <div class="form-group has-feedback">
                        <label>Telefono de Empresa</label>
                        <input type="text" class="form-control" placeholder="Telefono" name="txttel" id="tel" required>
                    </div>

                    <div class="form-group has-feedback">
                        <label>Dirección de Empresa</label>
                        <input type="text" class="form-control" placeholder="Dirección" name="txtdir" id="dir" required>
                    </div>

                    <div class="form-group has-feedback">
                        <label>Correo de Empresa</label>
                        <input type="email" class="form-control" placeholder="Correo" name="txtcor" id="cor" required>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-primary btn-block btn-flat" id="guardar">Guardar</button>
                        </div>
                    </div>
                </form> 
            </div>
        </div>
        <script src="../js/jquery-3.2.1.js"></script>
        <script src="../js/jquery-ui.min.js"></script>
        <script src="../js/alertify.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#guardar').click(function () {
                    $.post('../php/guardar_sucursal.php', {'nom': $('#nom').val(), 'tel': $('#tel').val(), 'dir': $('#dir').val(), 'due': $('#due').val(), 'cor': $('#cor').val()}, function (data) {
                        data=JSON.parse(data);
                        if(data.success){
                            window.location.href='login.php';
                        }
                    });
                });
            });
        </script>
    </body>
</html>

