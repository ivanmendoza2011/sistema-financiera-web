<?php
include '../php/utilidades.php';
include '../php/menu.php';

$combo = llenar_combobox2("select * from tipos where cod_tip='2' and est_tip=1;", "cod_tip_cod", "des_tip");

$tabla = "";
?>


<section class="content-header">
    <h1>
        Bancos <small>Panel de Control</small>
    </h1>
</section>

<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Clientes</h3>
        </div>
        <div class="box">
            <div class="box-body">
                <button type="button" class="btn btn-primary btn-flat" id="nuevo" data-toggle="modal" data-target="#MCliente">Nuevo</button>                
                <a href="../php/reportes/reporte_clientes.php" target="_blank" type="button" class="btn btn-success btn-flat">Imprimir Listado</a>
                
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table width="100%" id="example1" class="table table-bordered table-striped table-hover table-condensed">
                <thead>
                    <tr>
                        <th style="width: 10px;">Codigo</th>
                        <th>Nombre</th>
                        <th>Telefono</th>
                        <th style="width: 30px ;">Ced/Pasaporte</th>
                        <th>Correo</th>
                        <th style="width: 40px;">Estado</th>
                        <th style="width: 30px;">Acciones</th>
                    </tr>
                </thead>
                <tbody id="listacliente">
                    <?php if ($tabla != "") echo $tabla; ?>
                </tbody>
            </table>
        </div>
    </div>
    <!--Modal Cliente-->
    <div id="MCliente" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        Mantenimiento Cliente</h4>
                </div>
                <div class="modal-body">
                    <form role="form" class="form-horizontal" id="form_cliente">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="nom" class="col-sm-2 control-label">Nombre:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nom" placeholder="Nombre">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="ape" class="col-sm-2 control-label">Apellido:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="ape" placeholder="Apellido">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="doc" class="col-sm-2 control-label">CED/Pas.:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="doc" placeholder="CED/Pasaporte">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="tel" class="col-sm-2 control-label">Telefono:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="tel" placeholder="Telefono">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="cel" class="col-sm-2 control-label">Celular:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="cel" placeholder="Celular">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="cor" class="col-sm-2 control-label">Correo:</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="cor" placeholder="Correo">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="dir" class="col-sm-2 control-label">Dirección:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="2" id="dir" placeholder="Dirección..."></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="dir" class="col-sm-2 control-label">Estado:</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="cmb_estado" id="estado">
                                        <?php echo $combo; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-flat" id="btn_guardar">Guardar</button>
                    <button type="button" class="btn btn-default btn-flat" id="btn_limpiar">Limpiar</button>
                    <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
include '../php/script.php';
include '../php/footer.php';
?>

