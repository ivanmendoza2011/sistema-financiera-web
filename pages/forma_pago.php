<?php
include '../php/utilidades.php';
include '../php/menu.php';
?>

<section class="content-header">
    <h1>
        Registrar Formas de Pagos <small>Panel de Control</small>
    </h1>
</section>

<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Formas de Pago</h3>
        </div>
        <div class="box">
            <div class="box-body">
                <button type="button" class="btn btn-primary btn-flat" id="btnNuevo" data-toggle="modal">Nuevo</button>                
                <a id="btn_excel" target="_blank" type="button" class="btn btn-success btn-flat">Imprimir Listado</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table width="100%" id="tbFormaPago" class="table table-bordered table-striped table-hover table-condensed">
                <thead>
                    <tr>
                        <th style="width: 10px;">Codigo</th>
                        <th style="width: 800px;">Descripción</th>
                        <th style="width: 200px;">Cantidad de días</th>
                        <th style="width: 100px;">Estado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    
    <!--Modal Forma Pago-->
    <div id="MFormaPago" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
             <form method="post" action="" class="form-horizontal" id="form_forma_pago">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">
                            Mantenimiento de Formas de Pagos</h4>
                    </div>
                    <div class="modal-body">

                            <div class="box-body">                                
                                   
                                <div class="form-group">
                                    <label for="txtDesc" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="des_form" class="form-control" id="txtDesc" placeholder="Descripción de la Forma del Pago">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="txtCantDias" class="col-sm-2 control-label">Cantidad:</label>
                                    <div class="col-sm-10">
                                        <input name="cant_dias" type="text" class="form-control" id="txtCantDias" placeholder="Cantidad de días">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="slEstado" class="col-sm-2 control-label">Estado:</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="est_form" id="slEstado">
                                                                                    <?php echo llenar_combobox2("select * from tipos where cod_tip='2' and est_tip=1;", "cod_tip_cod", "des_tip"); ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-flat" id="btn_guardar">Guardar</button>
                        <button type="button" class="btn btn-default btn-flat" id="btnLimpiar">Limpiar</button>
                        <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div> <!-- Fin Modal -->

</section>


<?php
include '../php/script.php';
include '../php/footer.php';
?>

<script type="text/javascript" src="../pagesJS/forma_pago.js"></script>