<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FormaPagoDAO
 *
 * @author Wandy
 */

include __DIR__ . '/../ConexionBD.php'; // Asi se incluyen los archivos en la misma pagina
include '../interface_abstract/absCRUD.php';


class FormaPagoDAO extends absCRUD {
    
    const TABLA = 'forma_pago';
    private $con;
            
    function __construct() {
    
        $this->con = ConexionBD::getConexion();
    }
   
    public function delete() {
        
    }

    public function insert($fp) {
        
        $consulta = $this->con->prepararConsulta(
                'INSERT INTO ' . self::TABLA . ' (des_form, cant_dias, est_form) VALUES (:des_form, :cant_dias, :est_form)'
        );

        $consulta->bindParam(':des_form', $fp->getDesForm());
        $consulta->bindParam(':cant_dias', $fp->getCantDias());
        $consulta->bindParam(':est_form', $fp->getEstForm());

        $consulta->execute();          
    }

    public function select() {
        
        $consulta = $this->con->prepararConsulta(
                'SELECT *, CASE WHEN est_form = 1 THEN "Activo" ELSE "Desactivo" END AS estado FROM ' . self::TABLA 
        );
        
        $consulta->execute();
        
        return $this->con->get_array_data($consulta);
    }

    public function selectById($id) {
        
        $consulta = $this->con->prepararConsulta(
                'SELECT * FROM ' . self::TABLA . ' WHERE cod_form = :cod_form'
        );
        
        $consulta->bindParam(":cod_form", $id);
        $consulta->execute();
        
        return $this->con->get_array_data($consulta);
    }

    public function update($fp) {
        
        $consulta = $this->con->prepararConsulta(
                'UPDATE ' . self::TABLA . ' SET des_form = :des_form, cant_dias = :cant_dias, est_form = :est_form WHERE cod_form = :cod_form'
        );

        $consulta->bindParam(':des_form', $fp->getDesForm());
        $consulta->bindParam(':cant_dias', $fp->getCantDias());
        $consulta->bindParam(':est_form', $fp->getEstForm());
        $consulta->bindParam(':cod_form', $fp->getCodForm());
        
        $consulta->execute();          
    }

    public function getUltimoIdInsertado() {
        
        return $this->con->obtenerUltimoIdInsertado();
    }

}
