<?php
/**
 * Description of ConexionBD Aplicando patron Singleton para evitar la sobre carga al servidor
 *
 * @author Wandy
 */

// Reportar Errores
error_reporting(E_ALL);
ini_set('display_errors', 'On');

class ConexionBD {
    
    private $tipo_db = 'mysql';
    private $host = 'localhost'; // Nombre del servidor al que estamos conectado.
    private $nombre_db = 'prestamos';
    private $usuario = 'root';
    private $password = '1234';
    private $conexion = null;
    private static $instancia;
        
    private function __construct() {
        
        try {
            
            $this->conexion = new PDO($this->tipo_db.':host='.$this->host.';dbname='.$this->nombre_db, $this->usuario, $this->password);
            $this->conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conexion->exec('SET CHARACTER SET utf8');
            
        } catch (PDOException $ex) {
        
            echo 'Ha surgido un error y no se puede conectar a la base de datos. Detalles: ' . $ex->getMessage();
            die();            
        };
    }
    
    // Obtener la instancia existente, si no existe entonces se creará
    public static function getConexion() {
       
        if(!isset(self::$instancia)) {
            $nuevaConexion = __CLASS__;
            self::$instancia = new $nuevaConexion;
       }
       
       return self::$instancia;
    }
    
    // Evita que el objeto se pueda clonar
    public function __clone() {
        trigger_error('La clonación a este objeto no esta permitida.', E_USER_ERROR);
    }

    public function prepararConsulta($statement) {
        
        return $this->conexion->prepare($statement);
    }

    public function obtenerUltimoIdInsertado() {
        
        return $this->conexion->lastInsertId();
    }
    
    public function iniciarTransacion() {
        
        $this->conexion->beginTransaction();
    }
    
    public function iniciarCommit($s) {
        $this->commit();
    }
    
    public function iniciarRollback() {
        
        $this->rollback();
    }

    public static function cerrarConexion() {
        
        self::$conexion = null;
    }
    
    public static function get_array_data($consulta) {
        
        $data = array();      
        
        if(isset($consulta)){
            
            while($registro = $consulta->fetch()) {
                $data[] = $registro;
            }
            
            return $data;
        }else{
           return false;
        }

    }
    
    
    
}
