$(document).ready(function () {
    listar();
});

var cod = '';

$('#nuevo').click(function () {
    limpiar();
});

$('#btn_excel').click(function(e){
	window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#example1').html()));
    e.preventDefault();
});

$('#btn_guardar').click(function () {
    var nom = $('#nom').val().trim();
    var ape = $('#ape').val().trim();
    var doc = $('#doc').val().trim();
    var tel = $('#tel').val().trim();
    var cel = $('#cel').val().trim();
    var cor = $('#cor').val().trim();
    var dir = $('#dir').val().trim();
    var est = $('#estado').val().trim();

    $.ajax({
        method: "POST",
        url: "../php/cliente_salvar.php",
        data: {'cod': cod, 'nom': nom, 'ape': ape, 'doc': doc, 'tel': tel, 'cel': cel, 'cor': cor, 'dir': dir, 'est': est}
    }).done(function () {
        limpiar();
        $('#MCliente').modal('hide');
    });
});

$('#btn_limpiar').click(function () {
    limpiar();
});

function limpiar() {
    cod = '';
    $('#nom').val('');
    $('#ape').val('');
    $('#doc').val('');
    $('#tel').val('');
    $('#cel').val('');
    $('#cor').val('');
    $('#dir').val('');
    $('#estado').val(1);
    $('#nom').focus();

    //Recargar Datos a la tabla
    var table = $('#example1').DataTable();
    table.ajax.reload(function (json) {
    });

}

function listar() {
    var table = $('#example1').DataTable({
        //"destroy": true,
        ajax: {
            "method": "GET",
            "url": "../php/datos_clientes.php"
        },
        "columns": [
            {"data": "cod_cli"},
            {"data": "nom_cli"},
            {"data": "tel_cli"},
            {"data": "doc_cli"},
            {"data": "cor_cli"},
            {"data": "est_cli"},
            {
                sortable: false,
                "render": function () {
                    return '<button class="editar btn btn-primary  btn-xs"><i class="fa fa-pencil-square-o"></i> Editar</button>';
                }
            }
        ],
        responsive: true,
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_  Clientes",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en estos momentos",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ Clientes",
            "sInfoEmpty": "Mostrando Clientes del 0 al 0 de un total de 0 Clientes",
            "sInfoFiltered": "(filtrado de un total de _MAX_ Clientes)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    //Editar
    $('#example1 tbody').on('click', 'td button', function () {
        var data = table.row($(this).parents('tr')).data();

        cod = data.cod_cli;
        $('#nom').val(data.nom_cli2);
        $('#ape').val(data.ape_cli);
        $('#doc').val(data.doc_cli);
        $('#tel').val(data.tel_cli);
        $('#cel').val(data.cel_cli);
        $('#cor').val(data.cor_cli);
        $('#dir').val(data.dir_cli);
        $('#estado').val(data.est_cli2);

        $('#MCliente').modal("show");
    });
}
