
(function ($) {
    $(document).ready(onPageLoad);
    
    function onPageLoad() {

    // Confirmar que el navegador soporte Bootstrap y Jquery
    if (typeof jQuery === "undefined") { throw new Error("Bootstrap requires jQuery") }

    /** Catching Selectores del DOM*/
    var cod = 0;
    var $txtDesc = $('#txtDesc'); 
    var $txtCantDias = $('#txtCantDias');
    var $slEstado = $('#slEstado');
    var $tbFormaPago = $('#tbFormaPago');
    var $nuevo = $('#btnNuevo');
    var $limpiar = $('#btnLimpiar');
    var $mdFomarPago = $('#MFormaPago');
    
    iniciarConf();


    // Guardar datos
    $('#form_forma_pago').submit(function (e) {
    
        var data = prepararDataForm($(this), cod);
        
        $.ajax({
            url: '../business/logica/lgFormaPago.php',
            data: data,
            type: 'POST'
        }).done(function () {
            
            $mdFomarPago.modal('hide');
            recargarTabla();
            
        });
    
        e.preventDefault();
    });

    $nuevo.on('click', function (e){
     
        limpiarCampos();
        $mdFomarPago.modal('show');
        e.preventDefault();
    });
    
    $limpiar.on('click', function (e){
       
        limpiarCampos();
    });
    
    // Establece el Focus cuando abre el modal
    $mdFomarPago.on('shown.bs.modal', function () {
        $txtDesc.focus();
    });  
    
    /**	Metodos Genericos*/
    function iniciarConf() {

        listar();
    }
    
    // Generalizar este metodo
    function prepararDataForm($form, cod="")
    {
        var opc = '';
        if(cod > 0) opc = "u";
        else opc = "i";
        
        $form.append('<input hidden name="opc" id="txtOpc" value='+opc+'>');
        $form.append('<input hidden name="cod_form" id="txtCod" value='+cod+'>');
        
        var data = $form.serializeArray();
        
        return data;
    }
    
    function limpiarCampos()
    {
        $txtDesc.val('');
        $txtCantDias.val('');
        $slEstado.val('');
    }
    
    function recargarTabla()
    {
        $tbFormaPago.DataTable().ajax.reload(function (json) {});       
    }
        
        

function listar() {

    var table = $tbFormaPago.DataTable({
        //"destroy": true,
        "Processing": true,
        "ajax": {
            url: '../business/logica/lgFormaPago.php',
            data: {opc: 's'},
            type: 'POST'
        },       
        "columns": [
            {"data": "cod_form"},
            {"data": "des_form"},
            {"data": "cant_dias"},
            {"data": "estado"},
            {
                sortable: false,
                "render": function () {
                    return '<button class="editar btn btn-primary  btn-xs"><i class="fa fa-pencil-square-o"></i> Editar</button>';
                }
            }
        ],
        responsive: true,
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_  Formas de Pagos",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en estos momentos",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ Formas de Pagos",
            "sInfoEmpty": "Mostrando Formas de Pagos del 0 al 0 de un total de 0 Formas de Pagos",
            "sInfoFiltered": "(filtrado de un total de _MAX_ Formas de Pagos)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    //Editar
    $('#tbFormaPago tbody').on('click', 'td button', function () {
        var data = table.row($(this).parents('tr')).data();

        cod = data.cod_form;
        $txtDesc.val(data.des_form);
        $txtCantDias.val(data.cant_dias);
        $slEstado.val(data.est_form);

        $mdFomarPago.modal("show");
    });

}

 } // END LOAD PAGE
})(jQuery);